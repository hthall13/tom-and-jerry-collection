import re
import json
import requests
import warnings
from datetime import datetime
from bs4 import BeautifulSoup
from unidecode import unidecode


warnings.filterwarnings("error")


def parse_title(title_entry):
  title = ""
  wiki = ""

  if title_entry.find("i"):
    if title_entry.find("a"):
      wiki = base_wiki_url + title_entry.find("a")["href"]
      title = title_entry.find("a")["title"]
    elif title_entry.find(class_="vanchor-text"):
      title = title_entry.find(class_="vanchor-text").string
    else:
      title = title_entry.find("i").string
  else:
    title = title_entry.contents[0][:-1]
  return title, wiki, 


def get_characters(episode_number):
  spike_episodes = [5, 15, 16, 22, 26, 27, 33, 35, 42, 44, 53, 60, 62, 69, 72, 76, 78, 79, 82, 88, 91, 95, 104, 105, 109, 114, 149]
  mammy_episodes = [1, 2, 4, 5, 6, 10, 18, 28, 32, 36, 38, 39, 40, 48, 53, 58, 61, 67, 70]
  butch_episodes = [12, 23, 25, 32, 35, 42, 46, 48, 55, 57, 58, 79, 95, 101, 103, 108]
  tyke_episodes = [44, 53, 60, 62, 69, 72, 76, 78, 79, 82, 88, 91, 95, 104]
  nibbles_episodes = [24, 40, 51, 65, 78, 79, 83, 85, 89, 94, 107, 111, 113]
  toodles_episodes = [23, 26, 31, 55, 66, 95, 146]
  quacker_episodes = [47, 77, 87, 90, 97, 110, 112]
  characters = ["Tom Cat", "Jerry Mouse"]
  if episode_number != "#":
    episode_number = int(episode_number)
    if episode_number in spike_episodes:
      characters.append("Spike")
    if episode_number in mammy_episodes:
      characters.append("Mammy Two Shoes")
    if episode_number in butch_episodes:
      characters.append("Butch Isacc Cat")
    if episode_number in tyke_episodes:
      characters.append("Tyke")
    if episode_number in nibbles_episodes:
      characters.append("Nibbles Mouse")
    if episode_number in toodles_episodes:
      characters.append("Toodles Galore Lena Cat")
    if episode_number in quacker_episodes:
      characters.append("Quacker")
  return characters


def get_producer(year):
  if year == "Date":
    return ""
  elif int(year) < 1960:
    return "Hanna-Barbera/MGM"
  elif int(year) < 1963:
    return "Gene Deitch/Rembrandt Films"
  elif int(year) < 2001:
    return "Chuck Jones/Sib Tower 12"
  elif int(year) < 2005:
    return "Hanna-Barbera Productions/Turner Entertainment"
  else: return "Warner Bros. Animation"


if __name__ == '__main__':


  base_archive_url = "https://archive.org"
  base_wiki_url = "https://en.wikipedia.org"
  wiki_url = base_wiki_url + '/wiki/Tom_and_Jerry_filmography'
  wiki_html_text = requests.get(wiki_url).text
  wiki_soup = BeautifulSoup(wiki_html_text, 'html.parser')
  video_url = base_archive_url + '/details/tomandjerry_1080p'
  video_html_text = requests.get(video_url).text
  video_soup = BeautifulSoup(video_html_text, 'html.parser')
  thumbnail_url = base_archive_url + "/download/tomandjerry_1080p/tomandjerry_1080p.thumbs/"
  thumbnail_html_text = requests.get(thumbnail_url).text
  thumbnail_soup = BeautifulSoup(thumbnail_html_text, 'html.parser')

  arr = []
  for year in range(1940, 1959):
    arr.append({"year": str(year), "episodes": []})
  for year in range(1961, 1968):
    arr.append({"year": str(year), "episodes": []})
  for year in [2001, 2005, 2014, 2021]:
    arr.append({"year": str(year), "episodes": []})

  try:
    file_name_format = 'S[0-9]{4}E[0-9]{2} - .*\(.*\).mp4'
  except SyntaxWarning:
    print("excepted syntax warning")
  years = []
  episodes = []
  video_urls = []
  thumbnail_urls = []


  print("parsing years...")
  for title in wiki_soup.find_all('span', class_="mw-headline"):
    if len(title.string) == 4:
      years.append(title.string)


  videos = video_soup.find_all('a', class_="stealth download-pill", attrs={"href": re.compile(".mp4")})
  print("parsing video urls (%d entries)..." % len(videos))
  for video in videos:
    video_entry = video.text
    try:
      if re.search(file_name_format, video_entry) != None:
        videofile = re.search(file_name_format, video_entry).group()
        episode = {
          "decade": videofile[1:5],
          "decade_num": videofile[6:8],
          "title": videofile[11:videofile.index("(")-1],
          "url": video["href"]
        }
        video_urls.append(episode)
        # print(episode)
    except Exception as e:
      print(e)
  print("generated %d video entries" % len(video_urls))


  print("parsing thumbnail urls...")
  for thumbnail in thumbnail_soup.find_all('a', attrs={"href": re.compile(".jpg")}):
    if "1940" in thumbnail["href"] or "1950" in thumbnail["href"] or "1960" in thumbnail["href"]:
      thumb = {
        "title": thumbnail.text[11:thumbnail.text.index("(")-1],
        "url": thumbnail["href"]
      }
      thumbnail_urls.append(thumb)
      # print(thumb)


  print("parsing data tables...")
  tables = wiki_soup.find_all('table', class_="wikitable")
  for table in tables:

    for row in table.tbody.find_all('tr'):

      number = row.find(['th', 'td'])
      if number.string[:-1] != "No." and tables.index(table) <= 27:

        if tables.index(table) < 19 or tables.index(table) == 27:

          title_tag = number.find_next(['th', 'td'])
          date = title_tag.find_next(['th', 'td'])
          summary = date.find_next(['th', 'td'])
          notes = summary.find_next(['th', 'td'])

          summary_string = ""
          for item in summary.contents:
            summary_string += str(item)

        else:

          title_tag = number.find_next(['th', 'td'])
          date = title_tag.find_next(['th', 'td'])
          notes = date.find_next(['th', 'td'])
          summary_string = ""

        notes_string = ""
        for item in notes.contents:
          notes_string += str(item)

        title, wiki = parse_title(title_tag)

        episode = {
          "year": date.string[-5:-1],
          "number": number.string[:-1],
          "title": title,
          "date": datetime.strptime(date.string[:-1], "%B %d, %Y").strftime("%Y-%m-%d"),
          "summary": summary_string,
          "notes": notes_string[:-1],
          "producer": get_producer(date.string[-5:-1]),
          "characters": get_characters(number.string[:-1]),
          "wiki": wiki
        }
        episodes.append(episode)


  print("formatting episode data...")
  for episode in episodes:

    if episode["number"] != "#":

      for video in video_urls:
        if unidecode(video["title"].lower()).replace('-', ' ') in unidecode(episode["title"].lower()).replace('-', ' '):
          episode["video"] = base_archive_url + video["url"]

      for thumbnail in thumbnail_urls:
        if unidecode(thumbnail["title"].lower()).replace('-', ' ') in unidecode(episode["title"].lower()).replace('-', ' '):
          episode["thumbnail"] = thumbnail_url + thumbnail["url"]

      for year in arr:
        if episode["year"] == year["year"]:
          year["episodes"].append(episode)
  

  i = 0
  complete = []
  for year in arr:
    for episode in year["episodes"]:
      if "video" not in episode:
        i += 1
        print(str(i) + " --- " + episode["title"])
      else:
        complete.append(episode)
  

  with open("public/js/episode_list.js", "w") as write_file:
    write_file.write("let data = ")
    json.dump(complete, write_file, indent=4)
    write_file.write(";")
  # print(arr)