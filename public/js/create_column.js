const months = [
  "January", "February", "March", "April", "May", "June", "July",
  "August", "September", "October", "November", "December"
];
function create_column (episode) {
  let col = document.createElement("div");
  let card = document.createElement("div");
  let playimg = document.createElement("a");
  let img = document.createElement("img");
  let hovericon = document.createElement("button");
  let card_body = document.createElement("div");
  let card_title = document.createElement("h5");
  let card_subtitle = document.createElement("h6");
  // let card_text = document.createElement("p");
  let card_footer = document.createElement("div");
  let footer_buttons = document.createElement("div");
  let number_button = document.createElement("button");
  let year_button = document.createElement("a");
  let card_prod = document.createElement("small");
  // let card_note = document.createElement("p");
  
  // console.log("creating column for " + episode["title"] + "...");

  col.className = "col";
  card.className = "card shadow-sm";
  playimg.className = "container playimg";
  img.className = "bd-placeholder-img card-img-top";
  hovericon.className = "bg-transparent hovericon";
  card_body.className = "card-body";
  card_title.className = "card-title";
  card_subtitle.className = "card-subtitle mb-2 text-muted";
  // card_text.className = "card-text";
  // card_note.className = "card-text";
  card_footer.className = "d-flex justify-content-between align-items-center";
  footer_buttons.className = "btn-group";
  number_button.className = "btn btn-sm btn-outline-secondary disabled";
  year_button.className = "btn btn-sm btn-outline-secondary";
  card_prod.className = "text-muted";

  playimg.setAttribute("href", "stream.html?v=" + episode["number"]);
  img.setAttribute("src", episode["thumbnail"]);
  img.setAttribute("width", "100%");
  img.setAttribute("height", "225");
  number_button.setAttribute("type", "button");
  year_button.setAttribute("href", "stream.html?y=" + episode["year"]);
  year_button.setAttribute("style", "width:100%");

  let card_date_object = new Date(episode["date"]);
  let card_date_string = months[card_date_object.getMonth()] + " " + (card_date_object.getDate() + 1) + ", " + card_date_object.getFullYear();
  card_prod.appendChild(document.createTextNode(episode["producer"]));
  number_button.appendChild(document.createTextNode("#" + episode["number"]));
  year_button.appendChild(document.createTextNode(episode["year"]));
  // card_text.innerHTML = episode["summary"];

  footer_buttons.appendChild(number_button);
  footer_buttons.appendChild(year_button);

  card_footer.appendChild(footer_buttons);
  card_footer.appendChild(card_prod);

  if (episode["wiki"] != "") {
    let wiki_link = document.createElement("a");
    wiki_link.setAttribute("href", episode["wiki"]);
    wiki_link.appendChild(document.createTextNode(episode["title"]));
    card_title.appendChild(wiki_link);
  } else {
    card_title.appendChild(document.createTextNode(episode["title"]));
  }
  card_body.appendChild(card_title);
  card_subtitle.appendChild(document.createTextNode(card_date_string));
  card_body.appendChild(card_subtitle);
  // card_body.appendChild(card_text);
  // if (episode["notes"] != "") {
  //   card_note.innerHTML = episode["notes"];
  //   card_body.appendChild(card_note);
  // }
  card_body.appendChild(card_footer);

  playimg.appendChild(img);
  playimg.appendChild(hovericon);

  card.appendChild(playimg);
  card.appendChild(card_body);

  col.appendChild(card);

  // console.log("...done");
  
  return col;
};