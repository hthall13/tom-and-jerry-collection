const hb_tbl = document.getElementById("hb-mgm-episode-table");
const gd_tbl = document.getElementById("gd-rf-episode-table");
const cj_tbl = document.getElementById("cj-st12-episode-table");
const wb_tbl = document.getElementById("wb-episode-table");
const ids = ["hb-mgm", "gd-rf", "cj-st12", "wb", "search"];
const titles = [
  "Hanna-Barbera & MGM (1940-1958)",
  "Gene Deitch & Rembrandt Films (1961-1962)",
  "Chuck Jones & Sib Tower 12 (1963-1967)",
  "Hanna-Barbera/Turner & Warner Bros. Animation (2001-present)"
];
const descriptions = [
  "Hanna and Barbera produced 114 cartoons for MGM, thirteen of which were nominated for an Academy Award for Best Short Subject and seven went on to win, breaking the winning streak held by Walt Disney's studio in the category. Tom and Jerry won more Academy Awards than any other character-based theatrical animated series.",
  "In 1961, MGM revived the Tom and Jerry franchise, and contracted European animation studio Rembrandt Films to produce 13 Tom and Jerry shorts in Prague, Czechoslovakia. All were directed by Gene Deitch and produced by William L. Snyder. Deitch himself wrote most of the cartoons, with occasional assistance from Larz Bourne and Eli Bauer. Štěpán Koníček provided the musical score for the Deitch shorts.",
  "After the last of the Deitch cartoons were released, Chuck Jones, who had been fired from his 30-plus year tenure at Warner Bros. Cartoons, started his own animation studio, Sib Tower 12 Productions (later renamed MGM Animation/Visual Arts), with partner Les Goldman. Beginning in 1963, Jones and Goldman went on to produce 34 more Tom and Jerry shorts, all of which carried Jones' distinctive style (and a slight psychedelic influence).",
  "Warner Bros. Animation Inc.[2] is an American animation studio which is part of the Warner Bros. Television Studios, a division of Warner Bros., which is a subsidiary of Warner Bros. Discovery and serves as the animation division and label of Warner Bros. In recent years, Warner Bros. Animation has specialized in producing television and direct-to-video animation featuring characters from other properties owned by Warner Bros. Discovery, including Turner Entertainment (which owns the rights to properties originally created by the MGM cartoon studio), Hanna-Barbera, Ruby-Spears, and DC Entertainment."
];
const links = [
  {
    "name-1": "Hanna-Barbera",
    "link-1": "https://en.wikipedia.org/wiki/Hanna-Barbera",
    "name-2": "MGM",
    "link-2": "https://en.wikipedia.org/wiki/Metro-Goldwyn-Mayer_cartoon_studio",
    "link-3": "stream.html?p=MGM"
  },
  {
    "name-1": "Gene Deitch",
    "link-1": "https://en.wikipedia.org/wiki/Gene_Deitch",
    "name-2": "Rembrandt Films",
    "link-2": "https://en.wikipedia.org/wiki/Rembrandt_Films",
    "link-3": "stream.html?p=Rembrandt"
  },
  {
    "name-1": "Chuck Jones",
    "link-1": "https://en.wikipedia.org/wiki/Chuck_Jones",
    "name-2": "Sib Tower 12",
    "link-2": "https://en.wikipedia.org/wiki/MGM_Animation/Visual_Arts",
    "link-3": "stream.html?p=SibTower12"
  },
  {
    "name-1": "Hanna-Barbera/Turner Entertainment",
    "link-1": "https://en.wikipedia.org/wiki/Turner_Entertainment",
    "name-2": "Warner Bros. Animation",
    "link-2": "https://en.wikipedia.org/wiki/Warner_Bros._Animation",
    "link-3": "stream.html?p=Warner"
  }
];

data.forEach(episode => {
  let episode_card = create_column(episode);
  if (episode["producer"] == "Hanna-Barbera/MGM") {
    hb_tbl.appendChild(episode_card);
  } else if (episode["producer"] == "Gene Deitch/Rembrandt Films") {
    gd_tbl.appendChild(episode_card);
  } else if (episode["producer"] == "Gene Deitch/Rembrandt Films") {
    cj_tbl.appendChild(episode_card);
  } else {
    cj_tbl.appendChild(episode_card);
  }
});

function reveal(id) {
  // console.log("revealing " + id);
  ids.forEach(cur_id => {
    if (cur_id == id) {
      document.getElementById(cur_id + "-episode-table").style.display = "flex";
      if (cur_id != "search") {
        document.getElementById("info").style.display = "flex";
        document.getElementById(cur_id + "-tab").className = "nav-link active";
        document.getElementById("series-name").innerHTML = titles[ids.indexOf(cur_id)];
        document.getElementById("series-description").innerHTML = descriptions[ids.indexOf(cur_id)];
        document.getElementById("series-link-1").innerHTML = links[ids.indexOf(cur_id)]["name-1"];
        document.getElementById("series-link-1").href = links[ids.indexOf(cur_id)]["link-1"];
        document.getElementById("series-link-2").innerHTML = links[ids.indexOf(cur_id)]["name-2"];
        document.getElementById("series-link-2").href = links[ids.indexOf(cur_id)]["link-2"];
        document.getElementById("series-link-3").href = links[ids.indexOf(cur_id)]["link-3"];
      } else {
        document.getElementById("info").style.display = "none";
      }
    } else {
      document.getElementById(cur_id + "-episode-table").style.display = "none";
      if (cur_id != "search") {
        document.getElementById(cur_id + "-tab").className = "nav-link";
      }
    }
  });
}

reveal('hb-mgm')