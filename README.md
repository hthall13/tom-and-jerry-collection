# The Tom and Jerry Collection

Tom and Jerry is an American animated media franchise and series of comedy short films created in 1940 by William Hanna and Joseph Barbera. Best known for its 161 theatrical short films by Metro-Goldwyn-Mayer, the series centers on the rivalry between the titular characters of a cat named Tom and a mouse named Jerry. Many shorts also feature several recurring characters.

## Hanna-Barbera & MGM (1940-1958)

Hanna and Barbera produced 114 cartoons for MGM, thirteen of which were nominated for an Academy Award for Best Short Subject and seven went on to win, breaking the winning streak held by Walt Disney's studio in the category. Tom and Jerry won more Academy Awards than any other character-based theatrical animated series.

## Gene Deitch & Rembrandt Films (1961-1962)

In 1961, MGM revived the Tom and Jerry franchise, and contracted European animation studio Rembrandt Films to produce 13 Tom and Jerry shorts in Prague, Czechoslovakia. All were directed by Gene Deitch and produced by William L. Snyder. Deitch himself wrote most of the cartoons, with occasional assistance from Larz Bourne and Eli Bauer. Štěpán Koníček provided the musical score for the Deitch shorts.

## Chuck Jones & Sib Tower 12 (1963-1967)

After the last of the Deitch cartoons were released, Chuck Jones, who had been fired from his 30-plus year tenure at Warner Bros. Cartoons, started his own animation studio, Sib Tower 12 Productions (later renamed MGM Animation/Visual Arts), with partner Les Goldman. Beginning in 1963, Jones and Goldman went on to produce 34 more Tom and Jerry shorts, all of which carried Jones' distinctive style (and a slight psychedelic influence).
